/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import java.util.TreeMap;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 *
 * @author Karin
 */
public class AgeUtil {

    public static final TreeMap<Double, Integer> fractionToMonth;
    public static final TreeMap<Integer, Double> monthToFraction;
    public static final PeriodFormatter kidAgeFormatter;
    public static final PeriodFormatter adultAgeFormatter;

    static {
        fractionToMonth = new TreeMap<Double, Integer>();
        fractionToMonth.put(0.0, 0);
        fractionToMonth.put(0.084, 1);
        fractionToMonth.put(0.167, 2);
        fractionToMonth.put(0.250, 3);
        fractionToMonth.put(0.333, 4);
        fractionToMonth.put(0.417, 5);
        fractionToMonth.put(0.500, 6);
        fractionToMonth.put(0.583, 7);
        fractionToMonth.put(0.667, 8);
        fractionToMonth.put(0.750, 9);
        fractionToMonth.put(0.834, 10);
        fractionToMonth.put(0.917, 11);

        monthToFraction = new TreeMap<Integer, Double>();
        monthToFraction.put(0, 0.0);
        monthToFraction.put(1, 0.085);
        monthToFraction.put(2, 0.168);
        monthToFraction.put(3, 0.251);
        monthToFraction.put(4, 0.334);
        monthToFraction.put(5, 0.418);
        monthToFraction.put(6, 0.501);
        monthToFraction.put(7, 0.584);
        monthToFraction.put(8, 0.668);
        monthToFraction.put(9, 0.751);
        monthToFraction.put(10, 0.835);
        monthToFraction.put(11, 0.918);

        //Formatter for pretty age display for children
        kidAgeFormatter = new PeriodFormatterBuilder()
                .printZeroNever()
                .appendYears()
                .appendSuffix(" year ", " years ")
                .printZeroIfSupported()
                .appendMonths()
                .appendSuffix(" month", " months")
                .toFormatter();

        //Formatter for adults
        adultAgeFormatter = new PeriodFormatterBuilder()
                .printZeroNever()
                .appendYears()
                .appendSuffix(" year ", " years ")
                .toFormatter();
    }

    public static Double getNormalizedAge(Double age) {
        Double ageYear = Math.floor(age);
        return ageYear + normalizeAgeFraction(age - ageYear);
    }

    static Double normalizeAgeFraction(Double ageFraction) {
        return getAgeFraction(getAgeMonth(ageFraction));
    }

    static Double getAgeFraction(Integer month) {
        // 6 months old is at least 0.501 or more, so need to get the floor.
        return monthToFraction.floorEntry(month).getValue();
    }

    static Integer getAgeMonth(Double ageFraction) {
        return fractionToMonth.floorEntry(ageFraction).getValue();
    }

    static Integer getMinThreshold(Double ageFraction) {
		//Value returned needs to be greater than or equal to the threshold
        // For example, if an initiative start age is for .5 year old, the child needs to be at least .5 years old
        // So, need to get the ceiling
        return fractionToMonth.ceilingEntry(ageFraction).getValue();
    }

    static Integer getMaxThreshold(Double ageFraction) {
		// If the initiave end age is .5 year old, then child is not eligible if more than .5 years old
        // So need to get the floor
        return fractionToMonth.floorEntry(ageFraction).getValue();
    }

    public static Period ageToPeriod(Double age) {
        // Take the year part as integer
        Integer ageYear = Double.valueOf(Math.floor(age)).intValue();
        Integer ageMonth = getAgeMonth(age - ageYear);

        return new Period(ageYear, ageMonth, 0, 0, 0, 0, 0, 0, PeriodType.yearMonthDay());
    }

    public static Double dobToAge(DateTime dob) {
        Period currentAgePeriod = new Period(dob, new DateTime());
        Integer years = currentAgePeriod.getYears();
        return years + getAgeFraction(currentAgePeriod.getMonths());
    }

    public static Integer dobToAgeYear(DateTime dob) {
        Period currentAgePeriod = new Period(dob, new DateTime());
        return currentAgePeriod.getYears();
    }

    public static Boolean isDobWithinRange(DateTime dob, Double minAge, Double maxAge) {
        Period minAgePeriod = ageToPeriod(minAge);
        Period maxAgePeriod = ageToPeriod(maxAge);

        return dob.plus(maxAgePeriod).isAfterNow() && dob.plus(minAgePeriod).isBeforeNow();
    }
}
